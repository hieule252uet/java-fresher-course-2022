package design_pattern.observer;

public class Logger implements Observer {
    @Override
    public void update(User user) {
        System.out.println("LOGGER: " + user.toString());
    }
}

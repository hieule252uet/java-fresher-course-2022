package design_pattern.observer;

public interface Observer {
    void update(User user);
}

package design_pattern.observer;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        User user = new User("hieulx@slitigenz.io", "123456");
        UserService userService = new UserService(user);
        userService.attach(new Logger());

        userService.login("hieulx@slitigenz.io", "123");
        userService.logout();
    }
}

package design_pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class UserService implements Subject {
    private User user;
    private List<Observer> observerList = new ArrayList<>();

    public UserService(User user) {
        this.user = user;
    }

    @Override
    public void attach(Observer observer) {
        if (!observerList.contains(observer)) {
            observerList.add(observer);
        }
    }

    @Override
    public void detach(Observer observer) {
        if (observerList.contains(observer)) {
            observerList.remove(observer);
        }
    }

    @Override
    public void notifyAllObserver() {
        for (Observer observer : observerList) {
            observer.update(user);
        }
    }

    void login(String email, String password) {
        if (email.equals(this.user.getEmail()) && password.equals(this.user.getPassword())) {
            user.setAccountStatus(AccountStatus.LOGIN_SUCCESS);
            System.out.println("Dang nhap thanh cong");
        } else {
            user.setAccountStatus(AccountStatus.LOGIN_FAIL);
            System.out.println("Dang nhap that bai");
        }
        this.notifyAllObserver();
    }

    void logout() {
        if (user.getAccountStatus() == AccountStatus.LOGIN_SUCCESS) {
            user.setAccountStatus(AccountStatus.LOGOUT_SUCCESS);
            System.out.println("Dang xuat thanh cong");
        } else {
            user.setAccountStatus(AccountStatus.LOGIN_FAIL);
            System.out.println("Dang xuat that bai");
        }
        this.notifyAllObserver();
    }
}

package design_pattern.singleton;

public class DatabaseEarlySingleton {
    private DatabaseEarlySingleton(){}
    private static DatabaseEarlySingleton instance = new DatabaseEarlySingleton();

    private String tableName;
    public static DatabaseEarlySingleton getInstance() {
        return instance;
    }

    public void query() {
        System.out.println("SELECT * FROM " + tableName );
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}

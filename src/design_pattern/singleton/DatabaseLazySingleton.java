package design_pattern.singleton;

public class DatabaseLazySingleton {
    private DatabaseLazySingleton(){}
    private static DatabaseLazySingleton instance = null;

    private String tableName;
    public static DatabaseLazySingleton getInstance() {
        if(instance == null) {
            instance = new DatabaseLazySingleton();
        }
        return instance;
    }

    public void query() {
        System.out.println("SELECT * FROM " + tableName );
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}

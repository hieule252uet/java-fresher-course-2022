package design_pattern.singleton;

public class Main {
    public static void main(String[] args) {
        DatabaseLazySingleton databaseLazySingleton = DatabaseLazySingleton.getInstance();

        DatabaseLazySingleton databaseLazySingleton2 = DatabaseLazySingleton.getInstance();

        databaseLazySingleton.setTableName("USERS");
        databaseLazySingleton2.setTableName("PRODUCT");

        databaseLazySingleton.query();
        databaseLazySingleton2.query();

        if(databaseLazySingleton.hashCode() == databaseLazySingleton2.hashCode()) {
            System.out.println("Two objects point to the same memory location in the heap");
        }
        else  {
            System.out.println("Two objects don't point to the same memory location in the heap");
        }
    }
}

package design_pattern.adapter;

public class Main {
    public static void main(String[] args) {
        VietnameseAdaptee vietnameseAdaptee = new VietnameseAdaptee();

        TranslatorAdapter adapter = new TranslatorAdapter(vietnameseAdaptee);
        adapter.send("Xin chào");
    }
}

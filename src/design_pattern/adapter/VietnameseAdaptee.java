package design_pattern.adapter;

public class VietnameseAdaptee {
    public void receive(String words) {
        System.out.println("Receiving word....");
        System.out.println(words);
    }
}

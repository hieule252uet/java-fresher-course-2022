package design_pattern.adapter;

public interface JapaneseTarget {
    void send(String word);
}

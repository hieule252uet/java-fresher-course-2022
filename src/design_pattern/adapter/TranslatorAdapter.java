package design_pattern.adapter;

import java.util.HashMap;
import java.util.Map;

public class TranslatorAdapter implements JapaneseTarget {
    private VietnameseAdaptee adaptee;
    private Map<String, String> dictionary;

    public TranslatorAdapter(VietnameseAdaptee adaptee) {
        this.adaptee = adaptee;
        dictionary = new HashMap<>();
        dictionary.put("Xin chào", "やあ");
        dictionary.put("Tạm biệt", "さよなら");
    }


    @Override
    public void send(String word) {
        adaptee.receive(word);
        System.out.println("Translating...");
        String japaneseWord = this.translateToJapanese(word);
        System.out.println("Sending word...");
        System.out.println(japaneseWord);
    }

    private String translateToJapanese(String vietnameseWord) {
        return dictionary.get(vietnameseWord);
    }
}

package design_pattern.prototype;

public class Main {
    public static void main(String[] args) {

        Circle circle = new Circle();
        circle.X = 10;
        circle.Y = 10;
        circle.color = "white";
        circle.radius = 20;

        Circle copyCircle = (Circle) circle.clone();
        System.out.println(circle.toString());
        System.out.println(copyCircle.toString());
    }
}

package design_pattern.prototype;

public abstract class Shape implements Cloneable{
    int X;
    int Y;
    String color;

    public Shape() {
    }

    public Shape(Shape source) {
        this();
        this.X = source.X;
        this.Y = source.Y;
        this.color = source.color;
    }
}

package design_pattern.prototype;

public class Circle extends Shape {
    double radius;

    public Circle() {
    }

    public Circle(Circle source) {
        super(source);
        this.radius = source.radius;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Circle: { x: " + X + ", y: " + Y + ", color: " + color + ", radius: " + radius + " }";
    }
}

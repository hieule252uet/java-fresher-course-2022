package design_pattern.factory;

public class Main {
    public static void main(String[] args) {
        Transport truck = LogisticFactory.createTransport("truck");
        Transport ship = LogisticFactory.createTransport("ship");
        truck.delivery();
        ship.delivery();
    }
}

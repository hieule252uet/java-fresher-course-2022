package design_pattern.factory;

public interface Transport {
    void delivery();
}

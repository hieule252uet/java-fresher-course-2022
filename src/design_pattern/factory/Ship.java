package design_pattern.factory;

public class Ship implements Transport{
    @Override
    public void delivery() {
        System.out.println("Delivery by sea in a container");
    }
}

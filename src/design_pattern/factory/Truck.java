package design_pattern.factory;

public class Truck implements Transport{
    @Override
    public void delivery() {
        System.out.println("Delivery by land in a box");
    }
}

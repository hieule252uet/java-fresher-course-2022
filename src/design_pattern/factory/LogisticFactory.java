package design_pattern.factory;

public class LogisticFactory {
    public static Transport createTransport(String transportType) {
        switch (transportType) {
            case "truck":
                return new Truck();
            case "ship":
                return new Ship();
            default:
                return null;
        }
    }
}

package project1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Subject {
    private String name;
    private Teacher teacher;
    private List<Student> students;

    private List<ScoreRecord> scoreList;

    public Subject(String name) {
        this.name = name;
        students = new ArrayList<>();
        scoreList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<ScoreRecord> getScoreList() {
        return scoreList;
    }

    public void setScoreList(List<ScoreRecord> scoreList) {
        this.scoreList = scoreList;
    }

    public void updateScoreForStudent(Student student) {
        scoreList.add(new ScoreRecord((int) (Math.random() * 10), student));
    }

    public void showInformation() {
        System.out.println("-----------------------------------------------");
        System.out.println("Class: " + name);
        System.out.println("Teacher:" + teacher.toString());
        System.out.println("List students: ");
        students.forEach(student -> {
            System.out.println(student.toString());
        });
        System.out.println("-----------------------------------------------");
    }

    public void getTop3BestStudent() {
        scoreList.sort(new Comparator<ScoreRecord>() {
            @Override
            public int compare(ScoreRecord o1, ScoreRecord o2) {
                if (o1.getValue() == o2.getValue()) {
                    return (int) (o1.getStudent().getId() - o2.getStudent().getId());
                } else {
                    return (int) (o2.getValue() - o1.getValue());
                }
            }
        });
        for (int i = 0; i < 3; i++) {
            System.out.println(scoreList.get(i).getStudent().toString() + "; Score: " + scoreList.get(i).getValue());
        }
    }
}

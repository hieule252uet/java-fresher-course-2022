package project1;

import java.util.List;

public class MockData {
    public static final String[] firstNames = {"Nguyen", "Le", "Tran", "Pham", "Truong"};
    public static final String[] middleNames = {"Van", "Thi", "Xuan", "Thanh", "Anh"};
    public static final String[] lastNames = {"Hieu", "Long", "Linh", "Thao", "Dat"};

    public static final String[] grades = {"K1", "K2", "K3", "K4", "K5"};

    public static final String[] degrees = {"Master", "Doctor", "Professor"};

    public static final String[] subjectNames = {"Object Oriented Programming", "Database", "Web Development", "Mobile Development", "Java Programming"};
    public static long id = 0;

    public static Student getMockDataStudent() {
        id += 1000;
        String name = firstNames[(int) (Math.random() * 5)] + " " + middleNames[(int) (Math.random() * 5)] + " " + lastNames[(int) (Math.random() * 5)];
        String dateOfBirth = (int) (Math.random() * 30 + 1) + "/" + (int) (Math.random() * 12 + 1) + "/" + (int) (Math.random() * 5 + 1998);
        String grade = grades[(int) (Math.random() * 5)];
        return new Student(id, name, dateOfBirth, grade);
    }

    public static Teacher getMockDataTeacher() {
        id += 1000;
        String name = firstNames[(int) (Math.random() * 5)] + " " + middleNames[(int) (Math.random() * 5)] + " " + lastNames[(int) (Math.random() * 5)];
        String dateOfBirth = (int) (Math.random() * 30 + 1) + "/" + (int) (Math.random() * 12 + 1) + "/" + (int) (Math.random() * 1998 + 2004);
        String degree = grades[(int) (Math.random() * 3)];
        return new Teacher(id, name, dateOfBirth, degree);
    }
}

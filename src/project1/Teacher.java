package project1;

public class Teacher extends Person {
    private String degree;

    public Teacher(long id, String name, String dateOfBirth, String degree) {
        super(id, name, dateOfBirth);
        this.degree = degree;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    @Override
    void joinSubjectClass(Subject subject) {
        subject.setTeacher(this);
    }

    @Override
    public String toString() {
        return "Teacher : { id: " + getId() + ", name: " + getName() + ", birthDay: " + getDateOfBirth() + ", degree: " + degree + " }";
    }
}

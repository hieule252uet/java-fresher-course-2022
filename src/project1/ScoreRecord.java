package project1;

public class ScoreRecord {
    private double value;
    private Student student;

    public ScoreRecord(double value, Student student) {
        this.value = value;
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

package project1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Subject webDevelopment = new Subject("Web Development");
        Subject mobileDevelopment = new Subject("Mobile Development");

        Teacher teacher1 = MockData.getMockDataTeacher();
        Teacher teacher2 = MockData.getMockDataTeacher();

        teacher1.joinSubjectClass(webDevelopment);
        teacher2.joinSubjectClass(mobileDevelopment);

        List<Student> studentList = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Student student = MockData.getMockDataStudent();
            studentList.add(student);
            student.joinSubjectClass(webDevelopment);
            student.joinSubjectClass(mobileDevelopment);
        }

        for (int i = 0; i < webDevelopment.getStudents().size(); i++) {
            webDevelopment.updateScoreForStudent(webDevelopment.getStudents().get(i));
        }

        for (int i = 0; i < mobileDevelopment.getStudents().size(); i++) {
            mobileDevelopment.updateScoreForStudent(webDevelopment.getStudents().get(i));
        }

        webDevelopment.showInformation();
        webDevelopment.getTop3BestStudent();
    }
}

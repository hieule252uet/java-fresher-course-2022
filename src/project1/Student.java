package project1;

public class Student extends Person {
    private String grade;

    public Student(long id, String name, String dateOfBirth, String grade) {
        super(id, name, dateOfBirth);
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    void joinSubjectClass(Subject subject) {
        subject.getStudents().add(this);
    }

    @Override
    public String toString() {
        return "Student : { id: " + getId() + ", name: " + getName() + ", birthDay: " + getDateOfBirth() + ", grade: " + grade + " }";
    }
}

package project2;

import java.util.Scanner;

public class BMICalculator {
    public static double bmiCalculator(double height, double weight) {
        double bmi = weight / (height * height);
        return bmi;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao chieu cao cua ban: ");
        double height = sc.nextDouble();
        System.out.print("Nhap vao can nang cua ban: ");
        double weight = sc.nextDouble();

        double bmi = bmiCalculator(height, weight);
        System.out.println("-------------------------------");
        System.out.println("Chi so BMI cua ban la: " + Math.round(bmi * 100.0) / 100.0);
        if (bmi < 18.5) {
            System.out.println("Ban dang bi thieu can");
        } else if (bmi < 24.9) {
            System.out.println("Ban co co the can doi");
        } else if (bmi < 29.9) {
            System.out.println("Ban dang bi thua can");
        } else {
            System.out.println("Ban dang bi beo phi");
        }
    }
}
